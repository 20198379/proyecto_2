﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practica
{
    class Program
    {
        /*2.     Crear un programa que presente un menú con las siguientes opciones 

        o   Convertir grados a Celsius a Fahrenheit 

        o   Convertir dólar a pesos.

        o   Convertir metros a pies.

        o   Salir 

        Cada vez que finalice una de estas accione s debe regresar al menú. El programa  solo finalizará cuando el usuario elija la opción salir.*/

        public int convertir()
        {
            int b, r = 0;
            Console.WriteLine("Convertir grados Celsius °C a Fahrenheit °F.");
            Console.WriteLine("");
            Console.WriteLine("ingrese el valor °C:");
            b = Int32.Parse(Console.ReadLine());

            r = (b * 9 / 5) + 32;

            Console.WriteLine("°F:  " + r);
            return b;
        }
        public int convertir2()
        {
            int c, d = 0;
            Console.WriteLine("Convertir dolar a pesos.");
            Console.WriteLine("");
            Console.WriteLine("ingrese el valor Dolar $: ");
            c = Int32.Parse(Console.ReadLine());

            d = c * 56;

            Console.WriteLine("Peso $: " + d);
            return d;
        }
        public double convertir3()
        {
            double s, r = 0;
            Console.WriteLine("Convertir Metro a Pies.");
            Console.WriteLine("");
            Console.WriteLine("Ingresar el valor en M: ");
            s = Int32.Parse(Console.ReadLine());

            r = s * 3.281;

            Console.WriteLine( "Pies: "+r);
            return r;

        }

        public void menu()
        {
            ConsoleKeyInfo op;

            do
            {

                Console.WriteLine("BIENBENIDO");
                Console.WriteLine("");
                Console.WriteLine("A.Convertir grados Celsius a Fahrenheit.");
                Console.WriteLine("B.Convertir dolar a pesos.");
                Console.WriteLine("C.Convertir metros a pies.");
                Console.WriteLine("D.Salir");
                Console.WriteLine("ingrese la opcion deseada: ");
                op = Console.ReadKey(true);

                switch (op.Key)
                {
                    case ConsoleKey.A:
                        Console.Clear();
                        Program p = new Program();
                        p.convertir();
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case ConsoleKey.B:
                        Console.Clear();
                        Program d = new Program();
                        d.convertir2();
                        Console.ReadKey();
                        Console.Clear();

                        break;
                    case ConsoleKey.C:
                        Console.Clear();
                        Program z = new Program();
                        z.convertir3();
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case ConsoleKey.D:
                        Console.WriteLine("Cerrando Programa....");
                        break;
                    

                }
            } while (op.Key != ConsoleKey.D);


        }
        static void Main(string[] args)
        {
            Program p = new Program();
            p.menu();

            Console.ReadKey();
        }
    }
}
